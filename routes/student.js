const express = require("express");
const router = express.Router();
const middleware = require("../middleware/index.js");
const multer = require("multer");

const User = require("../models/user.js");
const Upload = require("../models/upload.js"); // Change variable name to 'Upload'


const storage = multer.memoryStorage();
const uploadMiddleware = multer({ storage: storage }).single("pdfFile");

router.get("/student/dashboard", middleware.ensureStudentLoggedIn, async (req,res) => {
	const studentId = req.user._id;
	const numPendingUpload = await Upload.countDocuments({ student: studentId }); 
	const numAcceptedUpload = await Upload.countDocuments({ student: studentId }); 
	res.render("student/dashboard", {
		title: "Dashboard",
		numPendingUpload, numAcceptedUpload,
	});
});



router.get("/student/upload", middleware.ensureStudentLoggedIn, (req,res) => {
	res.render("student/upload", { title: "Upload" });
});

router.post("/student/upload", middleware.ensureStudentLoggedIn, uploadMiddleware, async (req, res) => {
    console.log('req.body:', req.body);
  
    try {
      const { Cid, Name, email, percentage, course } = req.body;
  
      // Validate other fields
      if (!email || !course) {
        throw new Error("Email and Course are required fields");
      }
  
      // Check if an entry with the same CID already exists
      const existingUpload = await Upload.findOne({ Cid, student: req.user._id });
  
      if (existingUpload) {
        // If an entry exists, display the message and redirect
        req.flash("error", "You have already applied.");
        return res.redirect("back");
      }
  
      // Access the uploaded file using req.file
      // Example: console.log(req.file);
  
      // Process the file and save it to the database or perform other actions
      // ...
  
      // Save other fields to the database
      const newUpload = new Upload({
        student: req.user._id,
        Cid,
        Name,
        email,
        percentage,
        course,
        pdfFile: req.file.buffer.toString("base64"), // Assuming you want to store the file content as base64
      });
  
      await newUpload.save();
  
      req.flash("success", "Upload submitted successfully");
      res.redirect("/student/confirmation?Cid=" + Cid + "&Name=" + Name + "&email=" + email + "&percentage=" + percentage + "&course=" + course);
    } catch (err) {
      console.log(err);
      req.flash("error", "Some error occurred on the server.");
      res.redirect("back");
    }
  });
  

router.get("/student/confirmation", middleware.ensureStudentLoggedIn, (req, res) => {
    // Extract data from query parameters
    const { Cid, Name, email, percentage, course } = req.query;

    // Render the confirmation page and pass the data to the view
    res.render("student/confirmation", {
        title: "Confirmation",
        Cid,
        Name,
        email,
        percentage,
        course,
    });
});

router.get("/student/profile", middleware.ensureStudentLoggedIn, (req,res) => {
	res.render("student/profile", { title: "My Profile" });
});

router.put("/student/profile", middleware.ensureStudentLoggedIn, async (req,res) => {
	try
	{
		const id = req.user._id;
		const updateObj = req.body.student;	// updateObj: {firstName, lastName, gender, address, phone}
		await User.findByIdAndUpdate(id, updateObj);
		
		req.flash("success", "Profile updated successfully");
		res.redirect("/student/profile");
	}
	catch(err)
	{
		console.log(err);
		req.flash("error", "Some error occurred on the server.")
		res.redirect("back");
	}
});


// Function to fetch data for a specific course
async function fetchDataForCourse(studentId, course) {
    // Fetch data for the specified course
    const rankedData = await Upload.find({
        student: studentId,
        course: course,
    })
    .sort({ percentage: -1 }) // Sort by percentage in descending order (highest first)
    .select("Cid Name percentage"); // Select the fields you want to retrieve

    // Check if there is no data for the course
    if (rankedData.length === 0) {
        return { message: `You have not applied to ${course}.` };
    }

    return rankedData;
}

// Your middleware and other code here

router.get("/student/fetchRankedData/cs", middleware.ensureStudentLoggedIn, async (req, res) => {
    try {
        // Assuming you want to fetch data for the currently logged-in user
        const studentId = req.user._id;

        // Fetch data for Computer Science
        const computerScienceRankedData = await fetchDataForCourse(studentId, 'Computer Science');

        // Check for errors in the fetched data
        if (computerScienceRankedData.message) {
            // Handle the case where there's an error message or user hasn't applied
            return res.render("student/rankedData", {
                title: "Ranking for Computer Science",
                rankedData: computerScienceRankedData,
                course: 'Computer Science',
            });
        }

        // Render the Computer Science ranked data
        res.render("student/rankedData", {
            title: "Ranking for Computer Science",
            rankedData: computerScienceRankedData,
            course: 'Computer Science',
        });
    } catch (err) {
        console.error(err);
        req.flash("error", "Error fetching ranked data from the database.");
        res.redirect("back");
    }
});

router.get("/student/fetchRankedData/idd", middleware.ensureStudentLoggedIn, async (req, res) => {
    try {
        // Assuming you want to fetch data for the currently logged-in user
        const studentId = req.user._id;

        // Fetch data for Interactive Design and Development
        const interactiveDesignRankedData = await fetchDataForCourse(studentId, 'Interactive Design and Development');

        // Check for errors in the fetched data
        if (interactiveDesignRankedData.message) {
            // Handle the case where there's an error message or user hasn't applied
            return res.render("student/iddRankedData", {
                title: "Ranking for Interactive Design and Development",
                rankedData: interactiveDesignRankedData,
                course: 'Interactive Design and Development',
            });
        }

        // Render the Interactive Design and Development ranked data
        res.render("student/iddRankedData", {
            title: "Ranking for Interactive Design and Development",
            rankedData: interactiveDesignRankedData,
            course: 'Interactive Design and Development',
        });
    } catch (err) {
        console.error(err);
        req.flash("error", "Error fetching ranked data from the database.");
        res.redirect("back");
    }
});


module.exports = router;
