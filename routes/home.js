const express = require("express");
const router = express.Router();
// const Student = require("../models/upload")

router.get("/", (req,res) => {
	res.render("home/welcome");
});

router.get("/home/homewithoutlogin", (req,res) => {
	res.render("home/homewithoutlogin", { title: "Home | GCIT Registration System" });
});

router.get("/home/course1", (req,res) => {
	res.render("home/course1", { title: "Home | GCIT Registration System" });
});

router.get("/home/afterlogincourse", (req,res) => {
	res.render("home/afterlogincourse", { title: "Course | GCIT Registration System" });
});

router.get("/home/uiCourse", (req,res) => {
	res.render("home/uiCourse", { title: "Course | GCIT Registration System" });
});

router.get("/home/ueCourse", (req,res) => {
	res.render("home/ueCourse", { title: "Course | GCIT Registration System" });
});
router.get("/home/feCourse", (req,res) => {
	res.render("home/feCourse", { title: "Course | GCIT Registration System" });
});
router.get("/home/fsCourse", (req,res) => {
	res.render("home/fsCourse", { title: "Course | GCIT Registration System" });
});
router.get("/home/bCourse", (req,res) => {
	res.render("home/bCourse", { title: "Course | GCIT Registration System" });
});

router.get("/home/adCourse", (req,res) => {
	res.render("home/adCourse", { title: "Course | GCIT Registration System" });
});

router.get("/home/rank", (req,res) => {
	res.render("home/rank", { title: "Course | GCIT Registration System" });
});



module.exports = router;