const express = require("express");
const router = express.Router();
const middleware = require("../middleware/index.js");
const User = require("../models/user.js");
const upload = require("../models/upload.js");


router.get("/admin/dashboard", middleware.ensureAdminLoggedIn, async (req,res) => {
	const numAdmins = await User.countDocuments({ role: "admin" });
	const numStudents = await User.countDocuments({ role: "student" });
	const numPendingUpload = await upload.countDocuments({ status: "pending" });
	const numAcceptedUpload = await upload.countDocuments({ status: "accepted" });
	res.render("admin/dashboard", {
		title: "Dashboard",
		numAdmins, numStudents, numPendingUpload, numAcceptedUpload
	});
});

router.get("/admin/upload/pending", middleware.ensureAdminLoggedIn, async (req,res) => {
	try
	{
		const pendingUpload = await upload.find({status: ["pending", "accepted",]}).populate("student");
		res.render("admin/pendingUpload", { title: "Pending Upload", pendingUpload});
	}
	catch(err)
	{
		console.log(err);
		req.flash("error", "Some error occurred on the server.")
		res.redirect("back");
	}
});
router.get("/admin/upload/:uploadId", middleware.ensureAdminLoggedIn, async (req,res) => {
	try
	{
		const uploadId = req.params.uploadId;
		const upload = await upload.findById(uploadId).populate("student");
		res.render("admin/upload", { title: "upload details", upload });
	}
	catch(err)
	{
		console.log(err);
		req.flash("error", "Some error occurred on the server.")
		res.redirect("back");
	}
});

router.get("/admin/upload/accept/:uploadId", middleware.ensureAdminLoggedIn, async (req,res) => {
	try
	{
		const uploadId = req.params.donationId;
		await upload.findByIdAndUpdate(uploadId, { status: "accepted" });
		req.flash("success", "Marks upload successfully");
		res.redirect(`/admin/uploadmarks/view/${uploadId}`);
	}
	catch(err)
	{
		console.log(err);
		req.flash("error", "Some error occurred on the server.")
		res.redirect("back");
	}
});
router.get("/admin/profile", middleware.ensureAdminLoggedIn, (req,res) => {
	res.render("admin/profile", { title: "My profile" });
});

router.put("/admin/profile", middleware.ensureAdminLoggedIn, async (req,res) => {
	try
	{
		const id = req.user._id;
		const updateObj = req.body.admin;	// updateObj: {firstName, lastName, gender, address, phone}
		await User.findByIdAndUpdate(id, updateObj);
		
		req.flash("success", "Profile updated successfully");
		res.redirect("/admin/profile");
	}
	catch(err)
	{
		console.log(err);
		req.flash("error", "Some error occurred on the server.")
		res.redirect("back");
	}
	
});

router.get("/admin1/adminDashboard", middleware.ensureAdminLoggedIn, (req,res) => {
	res.render("/admin1/adminDashboard", { title: "My profile" });
});


module.exports = router;