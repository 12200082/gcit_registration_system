const mongoose = require("mongoose");

const uploadSchema = new mongoose.Schema({
    student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true
    },
    Cid: {
        type: Number,
        required: [true, 'Please provide a CID number!'],
        minlength: 11,
    },
    Name: {
        type: String,
        required: [true, 'Please tell us your name'],
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: true,
        validate: {
            validator: function (v) {
                // Basic email validation using a regex
                return /\S+@\S+\.\S+/.test(v);
            },
            message: props => `${props.value} is not a valid email address!`
        }
    },
    percentage: {
        type: Number,
        required: [true, 'Please provide a percentage scored!'],
        minlength: 0,
    },
    course: {
        type: String,
        required: [true, 'Please choose a course'],
        enum: ['Computer Science', 'Interactive Design and Development' /* Add more course options here */],
    },
    pdfFile: {
        type: String, // You might store the file path or URL here
        required: [true, 'Please upload a PDF file'],
    },
});

const Upload = mongoose.model("Upload", uploadSchema);
module.exports = Upload;
